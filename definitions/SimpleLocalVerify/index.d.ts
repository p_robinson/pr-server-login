import { Verify, IVerifyOptions } from '../Verify';
export interface ISimpleLocalVerifyOptions extends IVerifyOptions {
    checkPassword?: string;
}
export declare class SimpleLocalVerify extends Verify {
    constructor(User: any, options?: ISimpleLocalVerifyOptions);
    validate(username: string, password: string): boolean;
    login(username?: any, password?: any, done?: any): void;
    checkPassword(user: any, password: string): Promise<boolean>;
}
