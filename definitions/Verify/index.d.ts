export interface IVerifyOptions {
    findUser?: string;
}
export declare class Verify {
    User: any;
    options: any;
    constructor(User: any, options?: IVerifyOptions);
    findUser(identifier: string): any;
    login<T>(): void;
    activate(): any;
    onSuccess(user: any, done: any): void;
    onUserFail(done: any, message?: string): void;
}
