var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Verify_1 = require('../Verify');
var JWTVerify = (function (_super) {
    __extends(JWTVerify, _super);
    function JWTVerify(User, options) {
        if (options === void 0) { options = {}; }
        options.loadId = options.loadId || 'id';
        _super.call(this, User, options);
    }
    JWTVerify.prototype.login = function (jwt_payload, done) {
        var _this = this;
        this.findUser(jwt_payload[this.options.loadId])
            .then(function (user) {
            if (user) {
                _this.onSuccess(user, done);
            }
            else {
                _this.onUserFail(done);
            }
        })
            .catch(done);
    };
    return JWTVerify;
})(Verify_1.Verify);
exports.JWTVerify = JWTVerify;
