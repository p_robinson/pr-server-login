import * as _ from 'lodash';
export interface IVerifyOptions {
  findUser?: string
}

export class Verify {
  options: any = {
    findUser: 'findOne',
  };
  constructor(public User: any, options: IVerifyOptions = {}) {

    _.assign(this.options, options)
  }
  findUser (identifier: string) {
   return this.User[this.options.findUser](identifier)
  }
  login<T>() {}
  activate () {
    return this.login.bind(this)
  }
  onSuccess(user: any, done: any) {
    done(null, user)
  }
  onUserFail(done, message?: string) {
    done(null, false, { message: message })
  }
}
