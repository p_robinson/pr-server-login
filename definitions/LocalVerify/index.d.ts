import { SimpleLocalVerify, ISimpleLocalVerifyOptions } from '../SimpleLocalVerify';
export interface ILocalVerifyOptions extends ISimpleLocalVerifyOptions {
    incorrectPassword?: string;
}
export declare class LocalVerify extends SimpleLocalVerify {
    constructor(User: any, options?: ILocalVerifyOptions);
    login(username?: any, password?: any, done?: any): void;
    onIncorrectPassword(user: any): Promise<boolean | number>;
}
