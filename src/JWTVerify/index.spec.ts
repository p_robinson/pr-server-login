import { JWTVerify } from './';
import * as jsonwebtoken from 'jsonwebtoken'
var PassportJWT =  require('passport-jwt').Strategy;
import * as chai from 'chai';
chai.use(require('chai-passport-strategy'));

describe('JWTVerify', () => {

  var token: string;
  var User: any = {
    findOne: function (id) {
      return new Promise((resolve, reject) => {
        if (id === '1')
         {
          resolve({ user: id })
         }
         else {resolve(false)}
      })
    }
  };
  describe('Should Pass The Test With Correct Credentials ', () => {
    var user: any;
    before(done => {

      token = jsonwebtoken.sign({ id: '1' }, 'secret')
      chai.passport.use(new PassportJWT({ secretOrKey: 'secret' }, new JWTVerify(User).activate()))
        .success(u => {
          user = u;
          done()
        }).fail(done)
        .req(req => {
          req.headers.authorization = 'JWT ' + token
        })
        .authenticate()
    })
    it('Should find a user', done => {
      chai.expect(user).to.exist
      done()
    })
  })
  describe('Should Fail The Test With InCorrect Credentials ', () => {
    var user: any;
    before(done => {

      token = jsonwebtoken.sign({ id: '1' }, 'secret')
      chai.passport.use(new PassportJWT({ secretOrKey: 'secret' }, new JWTVerify(User).activate()))
        .success(u => {
          user = u;
          done(new Error())
        })
        .fail(message => {
          done()
        })
        .req(req => {
          req.headers.authorization = 'JWT ' + token + '1'
        })
        .authenticate()
    })
    it('Should not find a user', done => {
      chai.expect(user).to.not.exist
      done()
    })
  })

})
