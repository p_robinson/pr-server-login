import { Verify, IVerifyOptions } from '../Verify';

export interface ISimpleLocalVerifyOptions extends IVerifyOptions {
  checkPassword?: string;
}
export class SimpleLocalVerify extends Verify {
  constructor (User: any, options: ISimpleLocalVerifyOptions = {} ) {
    options.checkPassword = options.checkPassword || 'checkPassword'
    super(User, options)
  }
  validate(username: string, password: string) {
    return true
  }
  login(username?, password?, done?): void {

    if (this.validate(username, password)) {
      this.findUser(username)
        .then(user => {

          if (user) {
            this.checkPassword(user, password).then(isMatch => {
              if (isMatch) {
                this.onSuccess(user, done)
              }
              else {
                this.onUserFail(done, 'Passwords Do Not Match')
              }
            }).catch(done)
          }
          else {
            this.onUserFail(done, 'User Does Not Exist' )
          }
        }).catch(done)
    }
  }
  checkPassword(user: any, password: string): Promise<boolean> {
    return user[this.options.checkPassword](password)
  }
}
