import { SimpleLocalVerify, ISimpleLocalVerifyOptions } from '../SimpleLocalVerify';

export interface ILocalVerifyOptions extends ISimpleLocalVerifyOptions {
  incorrectPassword?: string;
}

export class LocalVerify extends SimpleLocalVerify {
  constructor(User, options: ILocalVerifyOptions = {}) {
    options.incorrectPassword = options.incorrectPassword || 'incorrectPassword'
    super(User, options)
  }
  login(username?, password?, done?) {
    if (this.validate(username, password)) {
      this.findUser(username)
        .then(user => {

          if (user) {
            this.checkPassword(user, password).then(isMatch => {
              if (isMatch) {
                this.onSuccess(user, done)
              }
              else {
                this.onIncorrectPassword(user)
                  .then(remainingAttempts => {

                    if (!remainingAttempts) {
                      this.onUserFail(done, 'Account Inactive')
                    }
                    else {
                      this.onUserFail(done, `${remainingAttempts} Attempts Remianing`)
                    }

                  })
              }
            }).catch(done)
          }
          else {
            done(null, false, { message: 'User Does Not Exist' })
          }
        }).catch(done)
    }
  }
  onIncorrectPassword(user: any): Promise<boolean | number> {
    return user[this.options.incorrectPassword]()
  }
}



