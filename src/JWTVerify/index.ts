import { Verify, IVerifyOptions } from '../Verify'

export interface SimpleLocalVerifyOptionsInterface extends IVerifyOptions {
  loadId?: string;
}
export class JWTVerify extends Verify {


  constructor (User, options: SimpleLocalVerifyOptionsInterface = {}) {
    options.loadId = options.loadId || 'id'
    super(User, options)

  }
  login(jwt_payload?: string, done?: any) {
    this.findUser(jwt_payload[this.options.loadId])
      .then(user => {
        if (user) {
          this.onSuccess(user, done)
        }
        else {
          this.onUserFail(done)
        }
      })
      .catch(done)
  }
}




