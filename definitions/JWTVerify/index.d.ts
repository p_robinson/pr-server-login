import { Verify, IVerifyOptions } from '../Verify';
export interface SimpleLocalVerifyOptionsInterface extends IVerifyOptions {
    loadId?: string;
}
export declare class JWTVerify extends Verify {
    constructor(User: any, options?: SimpleLocalVerifyOptionsInterface);
    login(jwt_payload?: string, done?: any): void;
}
