var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SimpleLocalVerify_1 = require('../SimpleLocalVerify');
var LocalVerify = (function (_super) {
    __extends(LocalVerify, _super);
    function LocalVerify(User, options) {
        if (options === void 0) { options = {}; }
        options.incorrectPassword = options.incorrectPassword || 'incorrectPassword';
        _super.call(this, User, options);
    }
    LocalVerify.prototype.login = function (username, password, done) {
        var _this = this;
        if (this.validate(username, password)) {
            this.findUser(username)
                .then(function (user) {
                if (user) {
                    _this.checkPassword(user, password).then(function (isMatch) {
                        if (isMatch) {
                            _this.onSuccess(user, done);
                        }
                        else {
                            _this.onIncorrectPassword(user)
                                .then(function (remainingAttempts) {
                                if (!remainingAttempts) {
                                    _this.onUserFail(done, 'Account Inactive');
                                }
                                else {
                                    _this.onUserFail(done, remainingAttempts + " Attempts Remianing");
                                }
                            });
                        }
                    }).catch(done);
                }
                else {
                    done(null, false, { message: 'User Does Not Exist' });
                }
            }).catch(done);
        }
    };
    LocalVerify.prototype.onIncorrectPassword = function (user) {
        return user[this.options.incorrectPassword]();
    };
    return LocalVerify;
})(SimpleLocalVerify_1.SimpleLocalVerify);
exports.LocalVerify = LocalVerify;
