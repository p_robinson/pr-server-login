import { LocalVerify } from './';
import { Strategy } from 'passport-local';
import * as chai from 'chai';
chai.use(require('chai-passport-strategy'))

var db = {
  findOne: function() {
        return new Promise((resolve, reject) => {
      var ret = {
        checkPassword: function(password) {
          return new Promise((resolve, reject) => {
            if (password === 'password') {
              resolve(true)
            }
            else {
              resolve(false)
            }
          })
        },
        incorrectPassword: function(password) {
          return new Promise((resolve, reject) => {
            resolve(1)
          })
        }
      }
      resolve(ret)
        })
  }
};

describe('passport-local using custom Local Strategy', () => {
  describe('Passing Correct Credentials', () => {

    var user: {};

    before(done => {
    chai.passport.use(new Strategy({ usernameField: 'username', passwordField: 'password' }, new LocalVerify(db).activate()))
        .success(u => {
          user = u;
          done()
        })
        .req(req => {
          req.body = { username: 'username', password: 'password' }
        })
        .authenticate()
    })
    it('Should Supply a user', (done) => {
      chai.expect(user).to.exist
      done()
    })
  })
  describe('Passing Incorrect Credentials', () => {
    var user: any;
    before(done => {
    chai.passport.use(new Strategy({ usernameField: 'username', passwordField: 'password' }, new LocalVerify(db).activate()))
      .success(u => {
        user = u;
        done()
      })
      .fail(info => {
        console.log(info)
        done()
      })
      .req(req => {
        req.body = { username: 'username', password: 'Password' }
      })
      .authenticate()
    })
    it('Should Not LocalStrategy User', done => {
      chai.expect(user).to.not.exist
      done()
    })
  })
})
