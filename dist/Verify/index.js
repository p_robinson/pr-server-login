var _ = require('lodash');
var Verify = (function () {
    function Verify(User, options) {
        if (options === void 0) { options = {}; }
        this.User = User;
        this.options = {
            findUser: 'findOne',
        };
        _.assign(this.options, options);
    }
    Verify.prototype.findUser = function (identifier) {
        return this.User[this.options.findUser](identifier);
    };
    Verify.prototype.login = function () { };
    Verify.prototype.activate = function () {
        return this.login.bind(this);
    };
    Verify.prototype.onSuccess = function (user, done) {
        done(null, user);
    };
    Verify.prototype.onUserFail = function (done, message) {
        done(null, false, { message: message });
    };
    return Verify;
})();
exports.Verify = Verify;
