var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Verify_1 = require('../Verify');
var SimpleLocalVerify = (function (_super) {
    __extends(SimpleLocalVerify, _super);
    function SimpleLocalVerify(User, options) {
        if (options === void 0) { options = {}; }
        options.checkPassword = options.checkPassword || 'checkPassword';
        _super.call(this, User, options);
    }
    SimpleLocalVerify.prototype.validate = function (username, password) {
        return true;
    };
    SimpleLocalVerify.prototype.login = function (username, password, done) {
        var _this = this;
        if (this.validate(username, password)) {
            this.findUser(username)
                .then(function (user) {
                if (user) {
                    _this.checkPassword(user, password).then(function (isMatch) {
                        if (isMatch) {
                            _this.onSuccess(user, done);
                        }
                        else {
                            _this.onUserFail(done, 'Passwords Do Not Match');
                        }
                    }).catch(done);
                }
                else {
                    _this.onUserFail(done, 'User Does Not Exist');
                }
            }).catch(done);
        }
    };
    SimpleLocalVerify.prototype.checkPassword = function (user, password) {
        return user[this.options.checkPassword](password);
    };
    return SimpleLocalVerify;
})(Verify_1.Verify);
exports.SimpleLocalVerify = SimpleLocalVerify;
